#include "ros/ros.h"
#include "simulated_car_detection/CylinderArray.h"
#include <sstream>
#include <Eigen/Core>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker");

  ros::NodeHandle n("~");
  std::string car_topic;
  if (!n.getParam("car_topic", car_topic)){
      ROS_ERROR_STREAM("Could not get car_topic parameter.");
      return -1;
  }
  int num_cars=0;
  if (!n.getParam("num_cars", num_cars)){
      ROS_ERROR_STREAM("Could not get num_cars parameter.");
      return -1;
  }

std::string car_x = "car_x";
std::string car_y = "car_y";
std::string car_z = "car_z";
std::string car_height = "car_height";

std::vector<Eigen::Vector3d> car_points;
std::vector<double> car_heights;

for(int i=0; i<num_cars; i++){
    double x;
    std::stringstream temp_x; temp_x<<car_x<<i;
    if (!n.getParam(temp_x.str(), x)){
        ROS_ERROR_STREAM("Could not get "<<temp_x.str()<<" parameter.");
        return -1;
    }
    double y;
    std::stringstream temp_y; temp_y<<car_y<<i;
    if (!n.getParam(temp_y.str(), y)){
        ROS_ERROR_STREAM("Could not get "<<temp_y.str()<<" parameter.");
        return -1;
    }
    double z;
    std::stringstream temp_z; temp_z<<car_z<<i;
    if (!n.getParam(temp_z.str(), z)){
        ROS_ERROR_STREAM("Could not get "<<temp_z.str()<<" parameter.");
        return -1;
    }

    double height;
    std::stringstream temp_height; temp_height<<car_height<<i;
    if (!n.getParam(temp_height.str(), height)){
        ROS_ERROR_STREAM("Could not get "<<temp_height.str()<<" parameter.");
        return -1;
    }

    Eigen::Vector3d v(x,y,z);
    car_points.push_back(v);
    car_heights.push_back(height);
}
  ros::Publisher car_pub = n.advertise<simulated_car_detection::CylinderArray>(car_topic, 1000);

  ros::Rate loop_rate(1);
  simulated_car_detection::CylinderArray c_arr;
  simulated_car_detection::Cylinder c;
  for(size_t i=0; i< num_cars;i++){
       c.position.x = car_points[i].x();
       c.position.y = car_points[i].y();
       c.position.z = car_points[i].z();

       c.height = car_heights[i];
       c_arr.cylinders.push_back(c);
  }
  int count = 0;
  while (ros::ok())
  {
    car_pub.publish(c_arr);
    ros::spinOnce();
    loop_rate.sleep();
    ++count;
  }

  return 0;
}
